# Documents

Bienvenue sur le Repo d'archive des differents documents produit
Cette archive comporte l'ensemble des documents produits au cours des differents semestre (presentations, rapport, reunions techniques, et autres)

Vous trouverez également, dans ce repo un document main expliquant brievement le projet

Pour toute demande de droit d'acces : damien.cartier-millon@insa-strasbourg.fr

**Nous vous conseillons vivement dans un premier temps de vous former a Git-GitLab pour eviter de faire de fausses manip**


Cheat Sheet Git : https://about.gitlab.com/images/press/git-cheat-sheet.pdf


Tutos youtube disponibles un peu partout 



**Pour les debutants l'application GitHub Desktop est bien pour debuter (Disponible en Cross platform)**


Certaines pages comportent des wikis, n'hesitez pas a les parcourir.

Hasta Luego, \
Damien 

<img src="https://i.imgur.com/DxzkI7e.png" width="50">
